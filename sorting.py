from program import print_list

numbers = [123,2132,23424,324234,4353,43]
# names = ["Alisher", "Argen", "Tilek", "Sabina", "meerim", "Bahhha"]
# d = sorted(d, reverse=True)
# print_list(d)
#
# print_list(sorted(names, reverse=True))

def my_sort(array):
    for i in range(len(array)):
        for j in range(i+1, len(array)):
            if array[j] < array[i]:
                array[i],array[j] = array[j],array[i]
    return array

print_list(my_sort(numbers))