def print_list(array):
	for item in array:
		print item

def main():
	names = []

	names.append("Sabina")
	names.append("Meerim")

	print_list(names)

	print len(names)

	names.remove("Sabina")

	print_list(names)